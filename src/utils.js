
// Returns boolean value from localStorage
export function checkLocalStorageBoolean(key){
    const result = JSON.parse(localStorage.getItem(key));
    if (result) {
        return result
    }
    return false;
}

export function removeElementFromArrayByKey(array, id){
    const result = array.filter(obj => obj.id !== id);
    return result;
}