import React from 'react';
import { Route, Switch } from 'react-router-dom';


import Home from 'components/HomePage/HomePage';
import Pictionary from 'components/PictionaryPage/PictionaryPage';
import Peliculas from 'components/PeliculasPage/PeliculasPage';
import CookiesPage from 'components/CookiesPage/CookiesPage';
import Page404 from 'components/404/404';

export default class Pages extends React.PureComponent {
    render(): JSX.Element {
        return (
            <div className="content">
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/pictionary" component={Pictionary} />
                    <Route path="/peliculas" component={Peliculas} />
                    <Route path="/cookies" component={CookiesPage} />
                    <Route component={Page404} />
                </Switch>  
            </div> 
        )
    }
}