import { createStore, combineReducers, Store } from 'redux';
import * as Reducers from './reducers/index';


export interface MainStore {
    words: Words.Object;
    movies: Movies.Object;
    game: Game.Object;
}

const store: Store<any> = createStore(
    combineReducers(
        {...Reducers}
    )
);

export default store;