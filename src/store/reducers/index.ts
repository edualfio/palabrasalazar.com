export { default as game } from 'components/Game/ducks/game';
export { default as words } from 'components/Words/ducks/words';
export { default as movies } from 'components/Movies/ducks/movies';