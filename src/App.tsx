import React from 'react';
import {  BrowserRouter as Router } from "react-router-dom";
import Pages from 'pages/Pages';
import Header from 'components/Header/Header';
import CookiesMessage from 'components/Cookies/Cookies';

import './App.css';


const App: React.FC = () => {
  return (
    <div className="App">
      <Router>
        <Header />
        <div className="wrapper">
          <Pages />
        </div>
        <CookiesMessage />
      </Router>
      
    </div>
  );
}

export default App;
