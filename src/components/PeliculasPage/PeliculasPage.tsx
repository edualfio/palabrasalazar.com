import React from 'react';
import Game from 'components/Game/container/Game';
import AdsComponent from 'components/Ads/Ads';
import Movies from 'components/Movies/container/Movies';

export default class Pictionary extends React.Component<any,any> {
    
    render(): JSX.Element {

        return (
            <section className='section'>
                <Game game={<Movies />} title='Las películas' instructions='lorem peliculas' />
                <AdsComponent />
            </section>
            
        )
    }
}

