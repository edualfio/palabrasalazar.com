import * as React from 'react';
import classNames from 'classnames'; 
import styles from  './Crono.module.scss';

const startSound = new Audio('/start.mp3');
const tickSound = new Audio('/tick.mp3');
const endSound = new Audio('/end.mp3');

interface CronoProps extends React.HTMLAttributes<HTMLDivElement> {
    type?:string;
    gameTime: number;
    onFinish?: () => void;
}

interface CronoState {
    time: number;
    timer: number;
    timesOver: boolean;
    timerTenLastSeconds: boolean;
}

export default class Crono extends React.PureComponent<CronoProps, CronoState> {
    constructor(props:any){
        super(props)
        this.state = {
            time: 10 * 1,
            timer: this.props.gameTime,
            timesOver: false,
            timerTenLastSeconds: false
        }

    }

    intervalId: any;

    private updateTimer = (seconds:any) => {

        tickSound.play();
        tickSound.volume =  0.2;
  
        if( seconds < 10) {
            this.setState({ timerTenLastSeconds: true });
            tickSound.volume = 0.7;
        } else {
            tickSound.volume = 0.2;
        }
        this.setState({
            timer: seconds
        });
    }

    public start = () => {
        let timer:any = this.props.gameTime;
        startSound.play()

        this.intervalId = window.setInterval(() => {
            if (--timer < 0) {
                this.stop();
            } else {
                this.updateTimer(timer);
            }
        }, 1000);

    }

    public stop = ():any => {
        clearInterval(this.intervalId);
        this.resetTimer();
        endSound.play();
        this.props.onFinish && this.props.onFinish();
    }

    private resetTimer = () => {
        this.setState({timerTenLastSeconds: false})
        this.updateTimer(this.props.gameTime);
    }

    render(): JSX.Element {
        const { timerTenLastSeconds } = this.state;
        return (
            <span className={classNames(timerTenLastSeconds && styles.ring)}>{this.state.timer}</span>
        )
    }
}
