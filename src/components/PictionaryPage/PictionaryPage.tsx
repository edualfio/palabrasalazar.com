import React from 'react';
import Game from 'components/Game/container/Game';
import AdsComponent from 'components/Ads/Ads';
import Words from 'components/Words/container/Words';

export default class Pictionary extends React.Component<any,any> {
    
    render(): JSX.Element {

        return (
            <section className='section'>
                <Game game={<Words />} title='Pictionary' instructions='lorem dibujar'  />
                <AdsComponent />
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam necessitatibus laborum eaque alias modi aspernatur rem praesentium asperiores aut voluptas culpa accusantium nam dignissimos fuga nemo dolorum nihil libero velit ratione pariatur, molestiae qui. Officia sit rem suscipit corporis architecto obcaecati eum qui corrupti deleniti sequi? Enim, quae. Optio, praesentium eligendi quas numquam nihil aliquam et nobis, laudantium mollitia minus odit sint maiores, cumque quia porro? Quisquam laborum iusto, similique ea inventore cupiditate adipisci quis, dolore, velit esse tempore? Ipsam, labore! Aliquid, odit. Non at rerum corrupti laboriosam, illum sequi nesciunt magnam corporis porro officiis ullam necessitatibus, alias dolor cum repellendus ipsam earum molestias voluptatibus, nulla eaque et optio nisi quas. Placeat, rerum. Consequuntur tempore consequatur sint dolorem nulla similique.
                </p>
            </section>
        )
    }
}

