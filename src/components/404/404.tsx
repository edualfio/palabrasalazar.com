import React from 'react';


export default class Page404 extends React.Component<any,any> {
    
    render(): JSX.Element {

        return (
            <section className='section'>
                <h1>Error 404</h1>
                ups! la página que buscas no existe
            </section>
            
        )
    }
}

