import * as React from 'react';
import './WordsComponent.scss';

interface State {
    reloaded: boolean;
}

export default class WordsComponent extends React.Component<any, State> {

    state: State = {
        reloaded: true
    }
    
    public reload() {
        this.setState({reloaded: !this.state.reloaded})

    }

    componentWillMount () {
        this.props.fetchNewWord()
        
    }

    render():JSX.Element{
        return (
            <div className='gameWrapper'>
                <div className='actualWord'>
                    {this.props.actualWord}
                </div>
            </div>
        );
        
    }

} 