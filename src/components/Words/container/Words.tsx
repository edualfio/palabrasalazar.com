import * as React from 'react';
import { connect } from "react-redux";
import { fetchNewWord } from "../ducks/words";
import WordsComponent from '../components/WordsComponent';


class WordsContainer extends React.Component<any, any> {
    render() {
        return <WordsComponent {...this.props} />
    }
}

const mapStateToProps = (state:any) => {
    const { words } = state;
    return { 
        actualWord: words.actualWord
    }
};

function mapDispatchToProps(dispatch:any) {
    return {
        fetchNewWord: () => dispatch(fetchNewWord({}))
    }
}

const Words = connect(mapStateToProps, mapDispatchToProps)(WordsContainer);
export default Words;