import { AnyAction } from "redux";
import data from 'data/words.json';

export enum ACTIONS { 
    FETCH_NEW_WORD = "FETCH_NEW_WORD"
}

export function fetchNewWord(payload: any) {
    return { 
        type: ACTIONS.FETCH_NEW_WORD, 
        payload 
    }
};

const initialState: Words.Object = {
    actualWord: '',
    wordsCount: 923,
    updated: false
};

function fetchNewWordFromDb(object:any) {
    const randomWord =  Math.floor(Math.random() * initialState.wordsCount) + 1;
    return data[randomWord].label;
    
}


export default function reducer(state:any = initialState, action: AnyAction) {
    switch(action.type){
        case ACTIONS.FETCH_NEW_WORD:
            console.log('entro2', state)
            return  {
                ...state,
                actualWord: fetchNewWordFromDb({}),
                updated: !state.updated
            };

        default: return state;
    }
};

