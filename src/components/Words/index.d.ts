declare namespace Words {
    export interface Object {
        actualWord: string;
        wordsCount: number;
        updated: boolean;
    }
}