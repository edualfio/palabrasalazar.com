import * as React from 'react';
import { connect } from "react-redux";
import { updateTeamsScore, updateRound, updateTeamName, updateShowSetup, updateGameTime, updateGameRounds, updateGameStarted, restartGame } from "../ducks/game";
import GameSetup from '../components/GameSetup';
import GameScore from '../components/GameScore';
import GameComponent from '../components/GameComponent';


class GameContainer extends React.Component<any, any> {
    
    render() {
        const { showSetup, gameStarted } = this.props;
        
        return (
            <>
                <GameSetup {...this.props}/>
                {!showSetup && <GameComponent {...this.props} />}
                {(gameStarted && !showSetup) && <GameScore {...this.props} /> }
            </>
        )
    }
}

const mapStateToProps = (state:any) => {
    const { game } = state;
    return { 
        teams: game.teams,
        teamsScore: game.teamsScore,
        gameTime: game.gameTime,
        actualTurn: game.actualTurn,
        actualRound: game.actualRound,
        roundLength: game.roundLength,
        showSetup: game.showSetup,
        gameStarted: game.gameStarted
    }
};

function mapDispatchToProps(dispatch:any) {
    return {
        updateTeamName: (id:any, name:string) => dispatch(updateTeamName({id:id, value:name})),
        updateGameTime: (time:number) => dispatch(updateGameTime({time})),
        updateGameRounds: (rounds: number) => dispatch(updateGameRounds({rounds})),
        updateTeamScore: (id:any) => dispatch(updateTeamsScore({id: id})),
        updateShowSetup: () => dispatch(updateShowSetup({})),
        updateGameStarted: (gameStarted:boolean) => dispatch(updateGameStarted({gameStarted})),
        restartGame: () => dispatch(restartGame({})),
        changeTurn: (id:any) => dispatch(updateRound({id: id}))
    }
}

const Game = connect(mapStateToProps, mapDispatchToProps)(GameContainer);
export default Game;