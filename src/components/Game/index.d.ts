declare namespace Game {
    export interface Object {
        teams: Array<string>;
        teamsScore: Array<number>;
        roundLength: number;
        actualTurn: number;
        updated: boolean;

    }
}