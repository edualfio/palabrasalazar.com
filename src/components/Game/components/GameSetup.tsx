import * as React from 'react';
import styles from './GameSetup.module.scss';
import { FiSettings, FiXOctagon } from 'react-icons/fi';


export default class GameSetup extends React.Component<any, any> {
    
    constructor(props:any){
        super(props);
        this.handleChangeTime = this.handleChangeTime.bind(this);
        this.handleChangeRounds = this.handleChangeRounds.bind(this);
        this.handleChangeTeamName = this.handleChangeTeamName.bind(this);
        this.handleChangeShowSetup = this.handleChangeShowSetup.bind(this);
    }

    handleChangeShowSetup(){
        this.props.updateShowSetup();
    }
    
    handleChangeTime(e:any) {
        this.props.updateGameTime(e.target.value);
    }

    handleChangeRounds(e:any) {
        this.props.updateGameRounds(e.target.value);
    }

    handleChangeTeamName(e:any) {
        this.props.updateTeamName(e.target.id, e.target.value);        
    }

    render():JSX.Element{
        const { title, gameTime, teams, showSetup, roundLength } = this.props;

        return (
            <>  
                <div className={styles.header}>
                   {title}
                    <span onClick={this.handleChangeShowSetup}>
                        {!showSetup ? <FiSettings /> : <FiXOctagon/>}
                    </span>
                </div>
                
                
                {showSetup && 
                <div className="setup">
                    <div className={styles.setupSection}>
                        <div className={styles.title}>
                            Nombre de los equipos:
                        </div>
                        <div  className={styles.settings}>
                            <input type="text" onChange={this.handleChangeTeamName} id='0' placeholder={teams[0]}/> <br/>
                            <input type="text" onChange={this.handleChangeTeamName} id='1' placeholder={teams[1]}/>
                        </div>
                        
                    </div>

                    <div className={styles.setupSection}>
                        <div className={styles.title}>
                            Tiempo por turno
                            <span>{gameTime}</span>
                        </div>
                        <div  className={styles.settings}>
                            <input className="slider is-fullwidth is-large is-circle is-info" onChange={this.handleChangeTime} step="10" min="0" max="120" value={gameTime} type="range"></input>
                        </div>
                    </div>

                    <div className={styles.setupSection}>
                        <div className={styles.title}>
                            Número de rondas
                            <span>{roundLength}</span>
                        </div>
                        <div  className={styles.settings}>
                            <input className="slider is-fullwidth is-large  is-circle is-info" onChange={this.handleChangeRounds} step="1" min="0" max="30" value={roundLength} type="range"></input>
                        </div>
                    </div>

                    <div className={styles.saveChanges}>
                        <span className="button is-info" onClick={this.handleChangeShowSetup}>Guardar cambios</span>
                    </div>
                </div>
                }
                
                </>

        )
    }

} 
