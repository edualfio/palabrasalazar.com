import * as React from 'react';
import classNames from 'classnames'; 
import styles from './GameScore.module.scss';

export default class GameScore extends React.Component<any, any> {

    checkWinner(teamsScore:Array<number>, teams:Array<string>) {
        const winner = teamsScore.indexOf(Math.max(...teamsScore));
        if(teamsScore[0] === teamsScore[1]) {
            return 'Empate';
        }
        return `¡Ha ganado el equipo ${teams[winner]} !`;
        
    }
  
    render():JSX.Element{
        const { teamsScore, teams, actualTurn, actualRound, roundLength } = this.props;
        const gameEnded = (actualRound - 1 === roundLength)
        
        return (
            <>
                <div>{gameEnded ? 'Resultado' : `Ronda ${actualRound} / ${roundLength}`}</div>

                {gameEnded && <div>{<span>{this.checkWinner(teamsScore, teams)}!</span>}</div>}

                <div className={styles.scoreTable}>
                    {teams.map((team:Array<string>, index:number) => (
                        <div className={classNames(index !== actualTurn && styles.disabled)} key={'tS_' + index}>
                            <div>
                                {team} 
                            </div>
                            <div>
                                {teamsScore[index]}
                            </div>
                        </div>
                    ))}
                </div>
            </>
        )

    }

} 
