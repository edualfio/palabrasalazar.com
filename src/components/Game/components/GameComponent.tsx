import * as React from 'react';
import { FiUsers } from 'react-icons/fi';

import Crono from 'components/Crono/Crono';
import styles from './Game.module.scss';

interface GameProps extends Game.Object, React.HTMLAttributes<HTMLDivElement> {
    //TODO
}

interface GameComponentState {
    roundStarted: boolean;
    timesOver: boolean;

}

export default class GameComponent extends React.Component<any, GameComponentState> {
    constructor(props:any) {
        super(props);
        this.cronoFinish = this.cronoFinish.bind(this);
    }

    // Inicializamos la instancia del crono
    cronoRef?: Crono;

    // Inicializamos la instancia del juego
    gameRef?: any;

    state: GameComponentState = {
        roundStarted: false,
        timesOver: false
    }


    startGame(){
        this.props.updateGameStarted(true);
        // !this.state.gameStarted && this.setState({ gameStarted: true }) 
    }

    restartGame(){
        this.props.restartGame();
        this.setState({
            roundStarted: false,
            timesOver: false
        });
    }

    startRound(){
        !this.state.roundStarted && this.setState({ roundStarted: true })
        this.roundStart()
    }

    endRound() {
        this.props.changeTurn(this.props.actualTurn);
        this.setState({
            roundStarted: false,
            timesOver: false
        })
    }

    roundStart(){
        this.cronoRef.start();
    }

    cronoFinish(){
        this.setState({ timesOver: true });
    }

    teamHit() {
        this.cronoRef.stop();
        this.props.updateTeamScore(this.props.actualTurn);
        this.endRound();
    }

    render():JSX.Element{
        const { teams, actualTurn, actualRound, game, gameTime, roundLength, gameStarted, instructions } = this.props;
        const { roundStarted, timesOver } = this.state;
        const teamInfo = teams[actualTurn];

        if ( !gameStarted ) {
            return (
                <div className={styles.gameStartWrapper}>
                    
                    <button className='button' onClick={() => this.startGame()}>Empezar a jugar</button>
                </div>
            );
        }

        if ( actualRound - 1 === roundLength ) {
            return (
                <>
                    <div>
                        el juego ha acabado!
                    </div>
                    <div className={styles.gameStartWrapper}>
                        <button className='button' onClick={() => this.restartGame()}>Jugar de nuevo</button>
                    </div>
                </>
            )
        }

        return (
            
            <div className={styles.gameWrapper}>
                <header>
                    <span className={styles.team}><FiUsers /> {teamInfo}</span>
                    <span className={styles.crono}>
                        <Crono
                            onFinish={this.cronoFinish}
                            gameTime={gameTime}
                            ref={(component: any) => { this.cronoRef = component }}/>
                    </span>
                    
                </header>
                <div className={styles.content}>
                    {!roundStarted && <h2>Preparados, listos, ....</h2>}
                    {!roundStarted && <button className="button is-info" onClick={() => this.startRound()}>Ya!</button>}
                    {roundStarted && game}
                    {timesOver && <h3 className={styles.result}>El tiempo ha acabado y parece que no habeis acertado :( </h3>}
                </div>
                <footer>
                    {timesOver && <button className="button is-danger" onClick={() => this.endRound()}>Cambio de turno!</button>}
                    {(roundStarted && !timesOver) && <button className="button is-success" onClick={() => this.teamHit()}>Hemos acertado!</button>}
                </footer>
            </div>

        )
    }

} 

     