import { AnyAction } from "redux";

export enum ACTIONS { 
    UPDATE_TEAMS_SCORE = "UPDATE_TEAMS_SCORE",
    UPDATE_TURN = "UPDATE_TURN",
    UPDATE_NAME = "UPDATE_NAME",
    UPDATE_GAME_TIME = "UPDATE_GAME_TIME",
    UPDATE_SHOW_SETUP = "UPDATE_SHOW_SETUP",
    UPDATE_ROUNDS = "UPDATE_ROUNDS",
    UPDATE_GAME_STARTED = "GAME_STARTED",
    RESTART_GAME = "RESTART_GAME"
}

export function updateGameStarted(payload: any){
    return {
        type: ACTIONS.UPDATE_GAME_STARTED,
        payload
    }
}

export function updateTeamsScore(payload: any) {
    return { 
        type: ACTIONS.UPDATE_TEAMS_SCORE, 
        payload 
    }
};

export function updateRound(payload: any) {
    return { 
        type: ACTIONS.UPDATE_TURN, 
        payload 
    }
};

export function updateTeamName(payload: any) {
    return {
        type: ACTIONS.UPDATE_NAME,
        payload
    }
}

export function updateGameTime(payload: any) {
    return {
        type: ACTIONS.UPDATE_GAME_TIME,
        payload
    }
}

export function updateShowSetup(payload: any) {
    return {
        type: ACTIONS.UPDATE_SHOW_SETUP,
        payload
    }
}

export function updateGameRounds(payload: any) {
    return {
        type: ACTIONS.UPDATE_ROUNDS,
        payload
    }
}

export function restartGame(payload: any) {
    return {
        type: ACTIONS.RESTART_GAME,
        payload
    }
}


const initialState: any = {
    teams: ['Team 1', 'Team 2'],
    teamsScore: [0, 0],
    gameTime: 60,
    roundLength: 10,
    actualRound: 1,
    actualTurn: 0,
    updated: false,
    gameStarted: false,
    showSetup: false
};

function changeTeamName(id: any, name:string, array:Array<string>) {
    array[id] = name;
    return array;
}

function updateTeamScore(id: any, teamsScore: any) {
    let array = teamsScore;
    array[id] = teamsScore[id] + 1
    return array;
}

function updateRoundNumber(round: number) {
    return ++round;
}

function changeTurn(id: any) {
    return id === 0 ? 1 : 0;
}

export default function reducer(state:any = initialState, action: AnyAction) {

    switch(action.type){
        case ACTIONS.UPDATE_GAME_STARTED:
            console.log('empieza el juego')
            return {
                ...state,
                gameStarted: action.payload.gameStarted,
                updated: !state.updated
            }
        case ACTIONS.UPDATE_TEAMS_SCORE:
            return  {
                ...state,
                teamsScore: updateTeamScore(action.payload.id, state.teamsScore),
                updated: !state.updated
            };

        case ACTIONS.UPDATE_TURN:
            console.log(state)
            return {
                ...state,
                actualTurn: changeTurn(action.payload.id),
                actualRound: action.payload.id === 1 ? updateRoundNumber(state.actualRound) : state.actualRound,
                updated: !state.updated
            }
        
        case ACTIONS.UPDATE_NAME:
            return {
                ...state,
                teams: changeTeamName(action.payload.id, action.payload.value, state.teams),
                updated: !state.updated
            }

        case ACTIONS.UPDATE_SHOW_SETUP:
            return {
                ...state,
                showSetup: !state.showSetup,
                updated: !state.updated
            }
        
        case ACTIONS.UPDATE_GAME_TIME:
            return {
                ...state,
                gameTime: action.payload.time,
                updated: !state.updated
            }
        
        case ACTIONS.UPDATE_ROUNDS:
            return {
                ...state,
                roundLength: action.payload.rounds,
                updated: !state.updated
            }
            
        case ACTIONS.RESTART_GAME:
            return {
                ...state,
                gameStarted: false,
                teamsScore: [0, 0],
                roundLength: 3,
                actualRound: 1,
                actualTurn: 0,
                updated: !state.updated

            }
            

        default: return state;
    }
};

