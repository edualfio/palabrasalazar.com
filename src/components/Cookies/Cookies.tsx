import React from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import styles from './Cookies.module.scss'; 

interface CookiesState {
    visible: boolean;
}

export default class CookiesMessage extends React.Component<any, CookiesState> {
    constructor (props:any){
        super(props);
        this.hideCookies = this.hideCookies.bind(this);
    }



    state: CookiesState = {
        visible: (localStorage.getItem('acceptCookies') === 'true' ? false : true)
    }

    hideCookies() {
        this.setState({
            visible: false
        })
        localStorage.setItem('acceptCookies', 'true');
    }

    render():JSX.Element {
        const { visible } = this.state;
        return (
           <div className={classNames(styles.cookies, visible && styles.visible)}>
                <p>Utilizamos cookies propias y de terceros para obtener datos estadísticos de la navegación de nuestros usuarios y mejorar nuestros servicios. Si acepta o continúa navegando, consideramos que acepta su uso. Puede cambiar la configuración u obtener más información <Link to='/cookies'>aquí</Link>.</p>
                <button onClick={this.hideCookies}>Entendido</button>
            </div>
        )
    }
}