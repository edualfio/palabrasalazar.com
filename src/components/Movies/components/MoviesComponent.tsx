import * as React from 'react';
import './MoviesComponent.scss';

interface State {
    reloaded: boolean;
}

export default class MoviesComponent extends React.Component<any, State> {

    state: State = {
        reloaded: true
    }
    
    public reload() {
        this.setState({reloaded: !this.state.reloaded})

    }

    componentWillMount () {
        console.log('>>>>>>>>>>>>><',this.props)
        this.props.fetchNewMovie()
        
    }

    render():JSX.Element{
        return (

            <div className='gameWrapper'>
                <div className='actualMovie'>
                    {this.props.actualMovie}
                </div>
            </div>
        );
        
    }

} 