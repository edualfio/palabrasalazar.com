declare namespace Movies {
    export interface Object {
        actualMovie: string;
        movieCount: number;
        updated: boolean;
    }
}