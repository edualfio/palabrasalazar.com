import { AnyAction } from "redux";
import data from 'data/movies.json';

export enum ACTIONS { 
    FETCH_NEW_MOVIE = "FETCH_NEW_MOVIE"
}

export function fetchNewMovie(payload: any) {
    return { 
        type: ACTIONS.FETCH_NEW_MOVIE, 
        payload 
    }
};

const initialState: Movies.Object = {
    actualMovie: '',
    movieCount: 500,
    updated: false
};

function fetchNewMovieFromDb(object:any) {
    const randomMovie =  Math.floor(Math.random() * initialState.movieCount) + 1;
    return data[randomMovie].label;
    
}


export default function reducer(state:any = initialState, action: AnyAction) {
    switch(action.type){
        case ACTIONS.FETCH_NEW_MOVIE:
            return  {
                ...state,
                actualMovie: fetchNewMovieFromDb({}),
                updated: !state.updated
            };

        default: return state;
    }
};

