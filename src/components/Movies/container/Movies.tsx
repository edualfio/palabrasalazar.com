import * as React from 'react';
import { connect } from "react-redux";
import { fetchNewMovie } from "../ducks/movies";
import MoviesComponent from '../components/MoviesComponent';


class MoviesContainer extends React.Component<any, any> {
    render() {
        return <MoviesComponent {...this.props} />
    }
}

const mapStateToProps = (state:any) => {
    const { movies } = state;
    return { 
        actualMovie: movies.actualMovie
    }
};

function mapDispatchToProps(dispatch:any) {
    return {
        fetchNewMovie: () => dispatch(fetchNewMovie({}))
    }
}

const Movies = connect(mapStateToProps, mapDispatchToProps)(MoviesContainer);
export default Movies;