import React from 'react';
import styles from './HomePage.module.scss';
import classNames from 'classnames'
import { Link } from 'react-router-dom';
import AdsComponent from 'components/Ads/Ads';
import { FiUsers, FiSettings, FiEdit2, FiAward } from 'react-icons/fi';


export default class Home extends React.Component<any,any> {
    

    render(): JSX.Element {

        return (
            <>
                <section className={classNames(styles.turq, styles.section, styles.header)}>
                    <h1 className={styles.title}>Juegos divertidos con palabras.</h1>
                    <div className={styles.bg}>
                    </div>
                </section>
                <section className={classNames(styles.yell, styles.section)}>
                    <Link className={styles.button} to='/pictionary'>Jugar al Pictionary</Link>
                    <Link className={styles.button} to='/peliculas'>Películas Aleatorias</Link>
                    {/* <Link className={styles.button} to='/palabras'>Generar Palabras</Link> */}
                
                    <p className={styles.homeText}>
                        Con nuestra aplicación, podrás encontrar palabras para juegos y divertirte con tus amigos. Pictionary, el juego de las películas, personajes. Todos estos juegos tienen generadores de palabras para juegos en fiestas con tus amigos y familiares
                    </p>
                    <AdsComponent />
                    <p className={styles.homeText}>
                        Además, para más diversión, incorporamos un cronómetro para controlar el tiempo de juego, tanto al dibujar como al intentar acertar las películas. No necesitarás más para disfrutar de los mejores juegos divertidos para adultos. En una cena, en una fiesta o en cualquier reunión con familiares.
                    </p>
                
                </section>
                <section className={classNames(styles.turq, styles.section, styles.rules)} >
                    <h2>¿Cómo se juega?</h2>
                        <div>
                            <h3><FiUsers /> Forma los equipos</h3>
                            <p>Elije los miembros de cada uno de los equipos. ¡No olvidéis preparar un cuaderno y un bolígrafo para dibujar!</p>
                        </div>
                        <div>
                            <h3><FiSettings /> Configura el juego</h3>
                            <p>Cambia los nombres de los equipos que van a jugar. Elije el tiempo de cada turno y selecciona el numero de ronda totales.</p>
                        </div>
                    
                        <div>
                            <h3><FiEdit2 /> Dibuja la palabra</h3>
                            <p>Dibuja en el cuaderno la palabra al azar que ha salido. ¡Usa tus mejores dotes artísticas! Si lo han adivinado, sumaréis un punto, en caso contrario, pasa el turno.</p>
                        </div>
                        <div>
                            <h3><FiAward /> El equipo que tenga más puntos al final de la última ronda... ¡habrá ganado!</h3>
                        </div>
                </section>
            </>
        )
    }
}

