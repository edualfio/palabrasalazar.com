import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Header.module.scss'; 

export default class Header extends React.Component<any, any> {

    
    render():JSX.Element {
        return (
            <header className={styles.header}>
                <Link to='/'><span>Palabras al Azar!</span></Link>
            </header>

        )
    }
}