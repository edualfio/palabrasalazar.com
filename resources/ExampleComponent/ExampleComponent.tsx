import React, { Component } from 'react';
class ExampleComponent extends Component<any, any> {
    constructor(props:any) {
        super(props);
        this.state = {
        articles: [
            { title: 'React Redux Tutorial for Beginners', id: 1 },
            { title: 'Redux e React: cosè Redux e come usarlo con React', id: 2 }
            ]
        };
    }
    render() {
        const { articles } = this.state;
        return <ul>{articles.map((el:any) => <li key={el.id}>{el.title}</li>)}</ul>;
    }
}

export default ExampleComponent;