import * as React from 'react';
import classNames from 'classnames'; 
import styles from  './Tools.module.scss';
import Crono from 'components/Crono/Crono';
import Score from 'components/Score/container/Score';
import { TiSpanner, TiStopwatch } from 'react-icons/ti';
import { checkLocalStorageBoolean } from 'utils';


interface ScoreProps extends React.HTMLAttributes<HTMLDivElement> {}

interface ScoreState {
    toolsIsVisible?: boolean;
}

const initialState: ScoreState = {

}

export default class Tools extends React.Component<ScoreProps, ScoreState> {
    
    state = initialState;
    
    toggleScore() {
        this.setState({toolsIsVisible: !this.state.toolsIsVisible}, () => {
            localStorage.setItem('scoreIsVisible', JSON.stringify(this.state.toolsIsVisible) )
        });
    }

    componentDidMount() {
        this.setState({
            toolsIsVisible: checkLocalStorageBoolean('toolsIsVisible')
        });
    }

    render(): JSX.Element {
        const { toolsIsVisible } = this.state;

        const toolsClass: string = classNames(
            styles.toolWrapper,
            toolsIsVisible && 
            styles.toolsVisible  
        );

        return (
            <div className={toolsClass}>
                <button className={styles.toogleTools} onClick={() => this.toggleScore()}><TiStopwatch /></button>
                <div className={styles.toolsContent}>
                    {/* <Crono /> */}
                </div>
            </div>  
        )
    }
}