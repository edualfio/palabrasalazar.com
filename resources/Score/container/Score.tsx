import * as React from 'react';
import { connect } from "react-redux";
import { updateTeamsScore } from "../../../store/actions/index";
import ScoreChart from '../components/ScoreChart';
import Score from '../components/Score'

class ScoreContainer extends React.Component<any, any> {

    render() {
        if (this.props.type === 'chart') {
            return <ScoreChart {...this.props} />
        } else {
            return <Score {...this.props} />
        }
    }
}

const mapStateToProps = (state:any):ScoreChart.StateToProps => {
    return { 
        teamsScore: state.teamsScore,
        teams: state.teams,
        updated: state.updated,
        actualTurn: state.actualTurn
    }
};

function mapDispatchToProps(dispatch:any) {
    return {
        updateTeamScore: (id:number) => dispatch(updateTeamsScore({id: id}))
    }
}

const ScoreElement = connect(mapStateToProps, mapDispatchToProps)(ScoreContainer);
export default ScoreElement;