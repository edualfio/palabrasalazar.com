import * as React from 'react';
import classNames from 'classnames'; 
import styles from  '../Score.module.scss';

export default class ScoreClass extends React.Component<any, any> {

    handleClick(id:number) {
        this.props.updateTeamScore(id);
    }

    render(): JSX.Element {
        const { teams } = this.props;

        return (
            <div className={styles.scoreWrap}>
                <ul>
                    { teams.map((team:string[], index:number) => (
                        <li className="list-group-item" key={index}>
                            {team} <button className={classNames(styles.teama, 'button is-primary')} onClick={() => this.handleClick(index)}>+1</button> 
                            {this.props.teamsScore[index]}
                        </li>
                    ))}
                </ul>

            </div>
        )
    }
}