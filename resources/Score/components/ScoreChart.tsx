import React from "react";
import classNames from 'classnames'; 
import styles from '../Score.module.scss';

export default class ScoreChartTemplate extends React.Component<any, any> {

    handleClick(id:number) {
        this.props.updateTeamScore(id);
    }

    isPlaying(index:number, actualTurn:number) {
        return index === actualTurn;
    }

    render() {
        const { teams, teamsScore, actualTurn, updated } = this.props;

        return (
            <>
                <h2>Puntuación</h2>
                <div className={styles.scoreTable}>
                    {teams.map((team:Array<string>, index:number) => (
                        <div className={classNames(!this.isPlaying(index, actualTurn) && styles.disabled)} key={'tS_' + index}>
                            <div >
                                {team}
                            </div>
                            <div>
                                {teamsScore[index]}
                            </div>
                            <div>
                                <button className={classNames(styles.teama, 'button is-primary')} onClick={() => { this.handleClick(index) }}>+1</button> 
                            </div>
                        </div>
                    ))}
                </div>
            </>
        )
    }
};
