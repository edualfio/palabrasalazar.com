import * as React from 'react';
import classNames from 'classnames'; 
import styles from  './Crono.module.scss';

const startSound = new Audio('/start.mp3');
const tickSound = new Audio('/tick.mp3');
const endSound = new Audio('/end.mp3');

interface CronoProps extends React.HTMLAttributes<HTMLDivElement> {
    type?:string;
    onFinish?: () => void;
}

interface CronoState {
    time: number;
    timer: string;
    timesOver: boolean;
    timerTenLastSeconds: boolean;
}


export default class Crono extends React.PureComponent<CronoProps, CronoState> {
    constructor(props:any){
        super(props)
        this.state = {
            time: 10 * 1,
            timer: '01:00',
            timesOver: false,
            timerTenLastSeconds: false
        }

        this.start = this.start.bind(this);
        this.stop = this.stop.bind(this);
        this.resetTimer = this.resetTimer.bind(this)
    }

    intervalId: any;

    private updateTimer = (minutes:any, seconds:any) => {

        tickSound.play();
        tickSound.volume =  0.2;
  
        if( seconds < 10) {
            this.setState({ timerTenLastSeconds: true });
            tickSound.volume = 0.7;
        } else {
            tickSound.volume = 0.2;
        }
        this.setState({
            timer: minutes + ":" + seconds
        });
    }

    public start = (duration:any) => {

        let timer:any = duration, minutes, seconds;

        // const updateTimer = (minutes:any, seconds:any) => this.updateTimer(minutes, seconds)
        // const stop = () => this.stop();

        startSound.play()

        this.intervalId = window.setInterval(() => {
            minutes = Math.floor(timer / 60);
            seconds = timer % 60;
    
            minutes = minutes < 10 ? '0' + minutes : minutes;
            seconds = seconds < 10 ? '0' + seconds : seconds;

            if (--timer < 0) {
                this.stop()
                this.setState({timerTenLastSeconds: false})

            } else {
                this.updateTimer(minutes, seconds);
            }
        }, 1000);
        console.log('crono start', this.intervalId)
    }

    public stop():any {
        const { onFinish } = this.props;

        this.resetTimer();
        endSound.play();
        clearInterval(this.intervalId);
        onFinish && onFinish();
    }

    private resetTimer = () => {
        this.setState({timerTenLastSeconds: false})
        console.log(this.state, this)
        this.updateTimer('01', '00');
    }

    render(): JSX.Element {
        const { timerTenLastSeconds } = this.state;
        return (
            <span className={classNames(timerTenLastSeconds && styles.ring)}>{this.state.timer}</span>
        )
    }
}
