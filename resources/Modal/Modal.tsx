import React from 'react';
import classNames from 'classnames';
import styles from './Modal.module.scss'; 
import Score from 'components/Score/container/Score'

export default class Modal extends React.Component<any,any> {

    state = {
        modalIsOpen: false
    }

    componentDidMount() {
        document.addEventListener('click', this.handleClickOutside, true);
    }
    
    
    handleClickOutside = (event:any) => {
        
        this.state.modalIsOpen && this.setState({modalIsOpen: false})
    }

    render(): JSX.Element {
        const { modalIsOpen } = this.state;

        const modalClass: string = classNames(
            styles.backdrop,
            modalIsOpen && 
            styles.active  
        );

        return (
            <div className={modalClass} >
                <div className={styles.modalWrapper}>
                    <h2>¿Ha acertado tu equipo?</h2>
                    <Score />
                </div>
            </div>
        )
    }
}

