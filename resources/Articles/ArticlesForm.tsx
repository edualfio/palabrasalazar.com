// src/js/components/Form.jsx
import React, { Component } from "react";
import { connect } from "react-redux";
import uuidv1 from "uuid";
// import { addArticle, updateArticleList } from "../../store/actions/index";

function mapDispatchToProps(dispatch:any) {
    return {
        // addArticle: (article:any) => dispatch(addArticle(article)),
        // updateArticleList: () => dispatch(updateArticleList({ updated: true}))
    };
}

class ConnectedForm extends Component<any, any> {

    state = {
        articleValue: ''
    }

    
    handleChange(event:any) {
        this.setState({ [event.target.id]: event.target.value });
    }
    handleSubmit(event:any) {
        event.preventDefault();
        const { articleValue }:any = this.state;
        const id = uuidv1();
        this.props.addArticle({ title: articleValue, id });
        this.props.updateArticleList();
        this.setState({ articleValue: '' });
    }
    render() {
        const { articleValue } = this.state;

        return (
            <form onSubmit={this.handleSubmit.bind(this)}>
                <input
                type="text"
                className="form-control"
                id="articleValue"
                value={articleValue}
                onChange={this.handleChange.bind(this)}
                />

                <button disabled={!Boolean(articleValue)} type="submit" className="btn btn-success btn-lg">
                    SAVE
                </button>
            </form>
        );
    }
}
const Form = connect(null, mapDispatchToProps)(ConnectedForm);
export default Form;