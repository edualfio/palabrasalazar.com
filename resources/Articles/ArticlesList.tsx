import React from "react";
import { connect } from "react-redux";
// import { removeArticle } from "../../store/actions/index";


const mapStateToProps = (state:any) => {
    return { 
        articles: state.articles, 
        updated: state.updated 
    };
};

function mapDispatchToProps(dispatch:any) {
    return {
        // removeArticle: (id:any) => dispatch(removeArticle({id: id})),
        
    };
}


const ConnectedList = ({ title, articles, updated, removeArticle }:any) => (
    <>
    <h2>{title}</h2>
    <ul className="list-group list-group-flush">
        <span>Is updated? -> {updated ? 'yes' : 'no'}</span>
        <br/>
        {articles.map((el:any) => (
            <li className="list-group-item" key={el.id}>
                <button onClick={() => removeArticle(el.id)}>X</button> {el.title} 
            </li>
        ))}
    </ul>
    </>
);

const List = connect(mapStateToProps, mapDispatchToProps)(ConnectedList);
export default List;