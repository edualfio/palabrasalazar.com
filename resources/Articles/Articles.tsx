import React from "react";
import List from "./ArticlesList";
const App = () => (
    <>
        <h2>Articles</h2>
        <List title='List of articles'/>
    </>
);
export default App;